using System;



namespace testapp.Models
{
    public class User{
        public string FullName { get; set; }
        public string Email { get; set; } 
        public DateTimeOffset BirthDate { get; set; }
        public string PassHash { get; set; }  
    }
}